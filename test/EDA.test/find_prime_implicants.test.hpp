#ifndef FIND_PRIME_IMPLICANTS_TEST_HPP
#define FIND_PRIME_IMPLICANTS_TEST_HPP
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <EDA/find_prime_implicants.hpp>

TEST(find_prime_implicants, no_input) {
  std::vector<std::vector<EDA::renameme>> inputs;
  EXPECT_THROW(auto temp = EDA::find_prime_implicants(inputs),
               std::runtime_error);
}
#endif

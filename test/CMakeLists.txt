cmake_minimum_required(VERSION 3.10)

project(unit_test)

add_executable(unit_test
    test.cpp
    EDA.test/find_prime_implicants.test.hpp
    ../external/googletest/googletest/src/gtest-all.cc
    ../external/googletest/googlemock/src/gmock-all.cc)

set_property (TARGET unit_test PROPERTY CXX_STANDARD 17)
set_property (TARGET unit_test PROPERTY LINKER_LANGUAGE CXX)

target_link_libraries(unit_test
    EDA
    pthread)

target_include_directories(unit_test
    PRIVATE
    ../external/googletest/googletest
    ../external/googletest/googletest/include
    ../external/googletest/googlemock
    ../external/googletest/googlemock/include
    ${EDA_INCLUDE_DIR}
)

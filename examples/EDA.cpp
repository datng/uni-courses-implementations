#include <EDA/find_prime_implicants.hpp>

#include <iostream>

using namespace EDA;

int main() {
  std::vector<std::vector<renameme>> a;
  a.push_back({renameme::zero, renameme::one, renameme::dont_care});
  a.push_back({renameme::one, renameme::one, renameme::dont_care});
  auto b = EDA::find_prime_implicants(a);
  for (size_t i = 0; i < b.size(); ++i) {
    for (size_t j = 0; j < b[i].size(); ++j) {
      std::cout << static_cast<int>(b[i][j]);
    }
    std::cout << std::endl;
  }
}

#include <iostream>
#include <string>
#include <vector>

namespace EDA {
enum class renameme { zero, one, dont_care };

[[nodiscard]] std::vector<std::vector<renameme>>
find_prime_implicants(std::vector<std::vector<renameme>> min_terms) {
  std::vector<std::vector<renameme>> output;
  if (!min_terms.size())
    throw std::runtime_error(std::string(__FILE__) + ":" +
                             std::to_string(__LINE__) + ": No minterm given.");
  size_t dimension = 0;
  for (auto &i : min_terms) {
    size_t temp = i.size();
    if (temp == 0)
      throw std::runtime_error(std::string(__FILE__) + ":" +
                               std::to_string(__LINE__) + ": Empty input.");
    if (dimension != 0 && temp != dimension)
      throw std::runtime_error(std::string(__FILE__) + ":" +
                               std::to_string(__LINE__) +
                               ": Minterms have different sizes.");
    dimension = temp;
  }

  size_t difference_count;
  size_t difference_index = dimension; // surely out of bound
  for (size_t id_pivot = 0; id_pivot < min_terms.size(); ++id_pivot) {
    for (size_t id_to_be_compared = id_pivot + 1;
         id_to_be_compared < min_terms.size(); ++id_to_be_compared) {
      difference_count = 0;
      for (size_t i = 0; i < dimension; ++i) {
        if (min_terms[id_pivot][i] != min_terms[id_to_be_compared][i]) {
          ++difference_count;
          difference_index = i;
        }
      }
      if (difference_count == 1) {
        std::vector<renameme> temp = min_terms[id_pivot];
        temp[difference_index] = renameme::dont_care;
        output.push_back(temp);
      }
    }
  }

  return output;
}
} // namespace EDA
